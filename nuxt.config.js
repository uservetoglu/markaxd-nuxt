const webpack = require("webpack");
const axios = require('axios');
module.exports = {
  mode: 'production',
  head: {
    htmlAttrs: {
      lang: 'tr'
    },
    title: 'Oto Paspas - 5D Paspas - Oto paspas fiyatları | MarkaXD',
    meta: [
      { charset: 'utf-8' },

      { name: 'google-site-verification', content: '_pdX-o41POk7JCLlg5kn9aQr-tR64lm7iMv6E1ylSF8' },
      { name: 'yandex-verification', content: '20d333b999967617'},

      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Oto paspas fiyatları, 5D paspas, havuzlu taban kaplama, 3D Deri paspas, Vip Koltuk Minderleri için MarkaxD. Güvenilir alışveriş ve en uygun fiyatlar'},
      { hid: 'og:url', property: 'og:url', content: 'https://markaxd.com' },
      { hid: 'og:title', property: 'og:title', content: '5D Havuzlu Paspas | MarkaXD Oto Paspas' },
      { hid: 'og:description', property: 'og:description', content: 'MarkaXD, aracınıza uygun paspas modelleri, özel tasarımlar ve uygun fiyata bulabileceğiniz tek adres' },
      { hid: 'twitter:title', property: 'twitter:title', content: '5D Havuzlu Paspas | MarkaXD Oto Paspas' },
      { hid: 'twitter:description', property: 'twitter:description', content: 'MarkaXD, aracınıza uygun paspas modelleri, özel tasarımlar ve uygun fiyata bulabileceğiniz tek adres' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/styles.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    './plugins/lodash.js',
    "./plugins/bootstrap.js",
    { src: '~/plugins/toast', mode: 'client' },
    { src: '~/plugins/instagram', mode: 'client' },
    { src: '~/plugins/modal', mode: 'client' },
    { src: '~/plugins/mask',  mode: 'client' },
    { src: '~/plugins/vue-social.js',ssr:false},
    { src: '~/plugins/lazyload.js',ssr:false},
    { src: '~/plugins/select',  mode: 'client' }
  ],

  env:{
/*    'api_url':'http://localhost:8000/api',
    'base_url':'http://localhost:8000',
    'cdn_url':'http://localhost:8000/',*/

    'api_url':'https://api.markaxd.com/api',
    'base_url':'https://api.markaxd.com',
    'cdn_url':'https://cdn.markaxd.com/',
    'instagram_access_token':'7003778572.cd3f634.7f6b086aabcc42c284edff8b36f94d45',
    'google_client_id' : '531604362349-j3c922pkd5ir81u7bm461b464i9im5i5.apps.googleusercontent.com',

  },


  modules: [
    [
      '@nuxtjs/sitemap',
      {
        exclude: [
          '/user/*',
          '/guest-address',
          '/address',
          '/cart',
          '/login',
          '/payment',
          '/registration',
          '/user',
        ],
        routes: async function () {

          let route =[]
            await axios.get('https://api.markaxd.com/api/sitemapCategory')
            .then(res =>{
              route = res.data;
            })
          return await route
        }
      }
    ],
    '@nuxtjs/robots',
    // With options
    ['nuxt-facebook-pixel-module', {
      /* module options */
      track: 'PageView',
      pixelId: '1051139555045545',
      disabled: false
    }]
  ],

  /*
** Nuxt.js modules
*/
  robots: [
    {
      UserAgent: 'Googlebot',
      Disallow: '/users',
    },
    {
      UserAgent: 'Bingbot',
      Disallow: '/admin',
    },
  ],
  /*
  ** Build configuration
  */
  build: {
    analyze: true,
    vendor: ['vue2-toast','jquery', 'bootstrap'],
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery"
      })
    ],
    extend (config, ctx) {
      if (ctx.isClient) {
        config.resolve.alias['vue'] = 'vue/dist/vue.js';
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          exclude: /(node_modules)/
        });
      }
    },
    extractCSS: false
  }
};
