import * as types from './mutation-types';
import BannerProxy from '@/proxies/BannerProxy';
import CategoryProxy from '@/proxies/CategoryProxy';
import ProductProxy from "@/proxies/ProductProxy";
import GlobalProxy from "@/proxies/GlobalProxy";
import CartProxy from "@/proxies/CartProxy";
import SessionProxy from "@/proxies/SessionProxy";
import PaymentProxy from "@/proxies/PaymentProxy";
import CargoProxy from "@/proxies/CargoProxy";

export const getAllBanners = ({ commit }, payload) => {

  let filter = payload && payload.filter ? payload.filter : {sort :'id' , order: 'asc', limit:5, offset:0 };

  return new BannerProxy(filter)
    .all()
    .then(response =>{
      commit(types.SET_DEFINITION, {key: 'banners', data: response.result.set});
      if(response.result.set) return response.result.pagination;
    });
};

export const getAllCategory = ({ commit }, payload) => {

  let filter = payload && payload.filter ? payload.filter : {sort :'id' , order: 'asc', limit:5, offset:0 };

  return new CategoryProxy(filter)
    .getTree()
    .then(response =>{
      commit(types.SET_DEFINITION, {key: 'categories', data: response.result.set});
      if(response.result.set) return response.result.pagination;
    });
};

export const getCategoryOfProduct = ({ commit }, payload) => {
  return new CategoryProxy()
    .categoryOfProduct(payload.category)
    .then(response =>{
      commit(types.SET_DEFINITION, {key: 'category', data: response.result.set});
      if(response.result.set) return response.result.pagination;
    });
};

export const getProduct = ({ commit }, payload) => {
  return new ProductProxy()
    .getProduct(payload.slug)
    .then(response =>{
      commit(types.SET_DEFINITION, {key: 'product', data: response.result.set});
      if(response.result.set) return response.result.pagination;
    });
};

export const getAllShowCase = ({ commit }, payload) => {

  let filter = payload && payload.filter ? payload.filter : {sort :'id' , order: 'asc', limit:5, offset:0 };

  return new CategoryProxy(filter)
    .getShowCase()
    .then(response =>{
      commit(types.SET_DEFINITION, {key: 'showCase', data: response.result.set});
      if(response.result.set) return response.result.pagination;
    });
};

export const getAllNewProducts =  ({ commit }, payload) =>{

  let filter = payload && payload.filter ? payload.filter : {sort :'id' , order: 'asc', limit:5, offset:0 };
  return new ProductProxy(filter)
    .newProducts()
    .then(response => {
      commit(types.SET_DEFINITION, {key: 'newProducts', data: response.result.set});
    });
};

export const getAllDiscountProducts =  ({ commit }, payload) =>{

  let filter = payload && payload.filter ? payload.filter : {sort :'id' , order: 'asc', limit:5, offset:0 };
  return new ProductProxy(filter)
    .discountProducts()
    .then(response => {
      commit(types.SET_DEFINITION, {key: 'discountProducts', data: response.result.set});
    });
};

export const instagramFeed =  ({ commit }, payload) =>{

  let filter = payload && payload.filter ? payload.filter : {sort :'id' , order: 'asc', limit:5, offset:0 };
  return new GlobalProxy(filter)
    .instagramFeed()
    .then(response => {
      commit(types.SET_DEFINITION, {key: 'instagramFeed', data: response.result.set});
    });
};

export const addCart = ({ commit }, payload) =>{
  return new Promise((resolve, reject) => {
    new SessionProxy()
      .session()
      .then((response) => {
        new Promise((resolve, reject) => {
          commit(types.SET_DEFINITION, {key: 'session', data: response.result.set.session});
          localStorage.setItem('session', response.result.set.session)
          resolve(response.result.set.session)
        })
          .then(() => {
            return new CartProxy()
              .add(payload)
              .then(response => {
                commit(types.SET_DEFINITION, {key: 'cart', data: response.result.set});
                if(response.code === 200){
                  localStorage.setItem('cart', JSON.stringify(response.result.set));
                }
                resolve(response)
              })
          })
      });
  })

};

export const removeCart = ({ commit }, payload) =>{
  return new Promise((resolve, reject) => {
    new SessionProxy()
      .session()
      .then((response) => {
        new Promise((resolve, reject) => {
          commit(types.SET_DEFINITION, {key: 'session', data: response.result.set.session});
          localStorage.setItem('session', response.result.set.session)
          resolve(response.result.set.session)
        })
          .then(() => {

            return new CartProxy()
              .remove(payload)
              .then(response => {
                commit(types.SET_DEFINITION, {key: 'cart', data: response.result.set});
                localStorage.setItem('cart', JSON.stringify(response.result.set));
                resolve(response.result.set)
              })
              .catch(error =>{
                reject(error)
              })
          })

      });
  })

};

export const removeCartItem = ({ commit }, payload) =>{
  return new Promise((resolve, reject) => {
    new SessionProxy()
      .session()
      .then((response) => {
        new Promise((resolve) => {
          commit(types.SET_DEFINITION, {key: 'session', data: response.result.set.session});
          localStorage.setItem('session', response.result.set.session)
          resolve(response.result.set.session)
        })
          .then(() => {
            return new CartProxy()
              .removeCartItem(payload)
              .then(response => {
                commit(types.SET_DEFINITION, {key: 'cart', data: response.result.set});
                localStorage.setItem('cart', JSON.stringify(response.result.set));
                resolve(response.result.set)
              })
          })
          .catch(error =>{
            resolve(error);
            commit(types.SET_DEFINITION, {key: 'cart', data: []});
            localStorage.setItem('cart', JSON.stringify([]));
            resolve(error)
          })
      });
  })

};

export const payment = ({ commit }, payload) =>{
  return new Promise((resolve, reject) => {
    new PaymentProxy()
      .payment(payload)
      .then(response => {
        resolve(response)
      }).catch((r) => {
        reject(r);
    });
  })
};

export const installment = ({ commit }, payload) =>{
  return new Promise((resolve, reject) => {
    new PaymentProxy()
      .installment(payload)
      .then(response => {
        resolve(response)
      });
  })
};

export const paymentDetail = ({ commit }, payload) =>{
  return new Promise((resolve, reject) => {
    new PaymentProxy()
      .paymentDetail(payload)
      .then(response => {
        resolve(response)
      });
  })
};

export const storageToState = ({ commit }, payload) =>{

  if (!process.server){
    let cart  = JSON.parse(localStorage.getItem('cart'));
    commit(types.SET_DEFINITION, {key: 'cart', data:cart});
  }
};

export const getCargoPrice = ({ commit }, payload) =>{

  return new Promise((resolve, reject) => {
    new CargoProxy()
      .getCargoPrice(payload)
      .then(response => {
        resolve(response)
      });
  })

};



export default {
  getAllBanners,
  getAllCategory,
  getAllShowCase,
  getAllNewProducts,
  getAllDiscountProducts,
  instagramFeed,
  addCart,
  removeCart,
  storageToState,
  removeCartItem,
  getCategoryOfProduct,
  getProduct,
  payment,
  getCargoPrice,
  paymentDetail,
  installment
};
