/* ============
 * State of the definition module
 * ============
 *
 * The initial state of the account module.
 */

export default {
    //Category
    categories: [],
    category: [],
    showCase: [],
    newProducts: [],
    products: [],
    product: [],
    orders: [],
    subCategories: [],

    discountProducts :[],
    banners: [],
    attributes: [],
    brands: [],
    coupons: [],

    shoppingBags: [],
    reviews: [],

    contents: [],
    instagramFeed: [],
    couponTypes: [{key: 'ca', label: 'Kategori'},{key: 'pr', label: 'Ürün'}],
    discountTypes: [{key: 'p', label: 'Fiyat'},{key: 'r', label: 'Oran'}],

    cart:[],

    user: [],
    token: null,
    address:[]
};
