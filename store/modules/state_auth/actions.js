import AuthProxy from '@/proxies/AuthProxy';
import * as types from './mutation-types';
export const getSession = ({ store,commit }, req) => {

	return new AuthProxy()
	.session()
	.then(response =>{
		commit(types.SET_DEFINITION, {key: 'session', data: response.result.set.session});
	});
};

export const checkLogin = ({ commit }, payload) =>{
	return new Promise((resolve, reject) => {
		return new AuthProxy(payload)
			.checkLogin()
			.then(response => {
				if(response.code === 200){
					commit(types.SET_DEFINITION, {key: 'user', data: response.result.set.user});
					resolve(response)
				}else{
					reject(response);
				}
			}).catch(err=>{
				localStorage.removeItem('token');
				localStorage.removeItem('address')
				reject(err);
			})
	});
};

export const login = ({ commit }, payload) =>{
	return new Promise((resolve, reject) => {
		return new AuthProxy(payload)
			.login(payload)
			.then(response => {
				if(response.code === 200){
					commit(types.SET_DEFINITION, {key: 'token', data: response.result.set.token});
					commit(types.SET_DEFINITION, {key: 'user', data: response.result.set.user});
					localStorage.setItem('token', response.result.set.token);
					localStorage.removeItem('address');
					resolve(response)
				}else{
					reject(response);
				}
			})
	});
};

export const register = ({ commit }, payload) =>{
	return new Promise((resolve, reject) => {
		return new AuthProxy(payload)
			.register(payload.data)
			.then(response => {
				if(response.code === 200){
					commit(types.SET_DEFINITION, {key: 'token', data: response.result.set.token});
					commit(types.SET_DEFINITION, {key: 'user', data: response.result.set.user});
					localStorage.setItem('token', response.result.set.token);
					localStorage.removeItem('address');
					resolve(response)
				}else{
					resolve(response);
				}
			}).catch(err =>{
				reject(err)
			})
	});
};


export const getAddress = ({ commit }, payload) =>{
	return new Promise((resolve, reject) => {
		return new AuthProxy(payload)
			.address()
			.then(response => {
				if(response.code === 200){
					commit(types.SET_DEFINITION, {key: 'address', data: response.result.set});
					resolve(response)
				}else{
					resolve(response);
				}
			}).catch(err =>{
				reject(err)
			})
	});
};

export const saveAddress = ({ commit }, payload) =>{
	return new Promise((resolve, reject) => {
		new AuthProxy(payload)
			.saveAddress(payload.data)
			.then(response => {
				if(response.code === 200){
					commit(types.SET_DEFINITION, {key: 'address', data: response.result.set});
					resolve(response)
					return response.result.set;
				}else{
					resolve(response);
				}
			}).catch(err =>{
			reject(err)
		})
	})
};


export const deleteAddress = ({ commit }, payload) =>{
	return new Promise((resolve, reject) => {
		new AuthProxy(payload)
			.deleteAddress(payload.id)
			.then(response => {
				if(response.code === 200){
					commit(types.SET_DEFINITION, {key: 'address', data: response.result.set});
					resolve(response)
				}else{
					resolve(response);
				}
			}).catch(err =>{
			reject(err)
		})
	})
};

export const updateProfile = ({ commit }, payload) =>{
	return new Promise((resolve, reject) => {
		new AuthProxy(payload)
			.updateProfile(payload.user)
			.then(response => {
				if(response.code === 200){
					commit(types.SET_DEFINITION, {key: 'user', data: response.result.set});
					resolve(response)
				}else{
					resolve(response);
				}
			}).catch(err =>{
			reject(err)
		})
	})
};

export const socialLogin = ({ commit }, payload) =>{
	return new Promise((resolve, reject) => {

		new AuthProxy(payload)
			.socialLogin(payload)
			.then(response => {
				if(response.code === 200){
					commit(types.SET_DEFINITION, {key: 'token', data: response.result.set.token});
					commit(types.SET_DEFINITION, {key: 'user', data: response.result.set.customer});
					localStorage.setItem('token', response.result.set.token);
					localStorage.removeItem('address');
					resolve(response)
				}else{
					resolve(response);
				}
			}).catch(err =>{
			reject(err)
		})
	})

};

export const getAllOrders = ({ commit }, payload) => {

	return new AuthProxy()
		.orders()
		.then(response => {

			commit(types.SET_DEFINITION, {key: 'orders', data: response.result.set});
		});
};

export default {
	login,
	checkLogin,
	register,
	getAddress,
	saveAddress,
	deleteAddress,
	socialLogin,
	getAllOrders,
	updateProfile
}
