/* ============
 * State of the definition module
 * ============
 *
 * The initial state of the account module.
 */

export default {
    session: null,
    authenticated: false,
    orders : []
};
