import Vue from 'vue';
import Vuex from 'vuex';
import state_definition from './modules/state_definition';
import state_auth from './modules/state_auth';
import createLogger from 'vuex/dist/logger';

Vue.use(Vuex)


const debug = process.env.NODE_ENV !== 'production';
const  createStore = ()=>{

  return new Vuex.Store({
    modules: {
      state_definition,
      state_auth,

    },
    strict: debug,
    plugins: debug ? [createLogger()] : [],
  });
};
export default createStore;