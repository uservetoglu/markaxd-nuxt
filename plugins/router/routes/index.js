// import auth from './auth';
// import admin from './admin';
import Index from '~/pages/index.vue';
import Category from '~/pages/Category/index.vue';

const routes = [
  {
    path: '/',
    component: Index,
    redirect: '/',
  },
  {
    path: '/:category',
    name: 'category',
    component: Category,
  },
];

export default routes;
