import Proxy from './BaseProxy';
import Vue from 'vue';
import axios from "axios";

class AuthProxy extends Proxy {
  constructor(parameters = {}) {
    super('', parameters);
  }

  session(){
    return this.submit('get', `/session`);
  }

  login(data) {
    return this.submit('post', '/login', data);
  }

  socialLogin(data) {
    return this.submit('post','/social-login', data);
  }

  register(data) {
    return this.submit('post', '/register', data);
  }

  address() {
    return this.tokenSubmit( '/customer/addresses');
  }

  saveAddress(data){
    return this.submit('post', '/customer/addresses', data);
  }

  deleteAddress(data){
    return this.tokenDelete('/customer/addresses/'+data);
  }

  checkLogin() {
    return this.tokenSubmit('/check-login');
  }

  orders() {
    return this.tokenSubmit( '/customer/order');
  }

  updateProfile(data) {
    return this.tokenPut( '/update-profile',data);
  }

  tokenDelete(url){

    const headers = {
      'Authorization': 'Bearer '+localStorage.getItem('token'),
    };
    return new Promise((resolve, reject)=>{
      axios.delete(this.BaseUrl+url, {headers})
        .then(response => {
            resolve(response.data);
          }
        )
        .catch(
          error=>{
            reject(error)
          }
        );
    })
  }

  tokenSubmit(url){

    const headers = {
      'Authorization': 'Bearer '+localStorage.getItem('token'),
    };
    return new Promise((resolve, reject)=>{
      axios.get(this.BaseUrl+url, {headers})
          .then(response => {
                resolve(response.data);
              }
          )
          .catch(
              error=>{
                reject(error)
              }
          );
    })
  }

  tokenPut(url, data){

    const headers = {
      'Authorization': 'Bearer '+localStorage.getItem('token'),
    };
    return new Promise((resolve, reject)=>{
      axios.put(this.BaseUrl+url, data, {headers})
        .then(response => {
            resolve(response.data);
          }
        )
        .catch(
          error=>{
            reject(error)
          }
        );
    })
  }

  submit(requestType, url, data = null) {

    const headers = {
        'Authorization': 'Bearer '+localStorage.getItem('token'),
    };

    return new Promise((resolve, reject) => {
      axios[requestType](this.BaseUrl+url,data,  {headers})
        .then((response) => {
          resolve(response.data);
        })
        .catch(({response}) => {
          if (response) {
            reject(response.data);
          } else {
            reject();
          }
        });
    });
  }
}

export default AuthProxy;
