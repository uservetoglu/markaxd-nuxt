import Proxy from './BaseProxy';
import axios from "axios";

class SessionProxy extends Proxy {
	constructor(parameters = {}) {
		super('/', parameters);
	}

	session(){
		return this.submit('get', `/session`);
	}

	submit(requestType, url, data = null){
		const headers = {
			'session': localStorage.getItem('session'),
		};
		return new Promise((resolve, reject)=>{
			axios[requestType](this.BaseUrl+url+this.getParameterString(), {headers})
			.then(
				response =>{
					resolve(response.data);
				}
			)
			.catch(
				error=>{
					reject(error)
				}
			);
		})
	}

}

export default SessionProxy;
