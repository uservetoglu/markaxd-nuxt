import Proxy from './BaseProxy';

class CategoryProxy extends Proxy {
	constructor(parameters = {}) {
		super('category', parameters);
	}

	getTree(){
		return this.submit('get', `/site-menu`);
	}
	getShowCase(){
		return this.submit('get', `/category/vitrin`);
	}
	categoryOfProduct(category){
		return this.submit('get', `/category/category-of-product/`+category);
	}

	subCategories(parent){
		return this.submit('get', `/category/sub-categories/`+parent);
	}
}

export default CategoryProxy;
