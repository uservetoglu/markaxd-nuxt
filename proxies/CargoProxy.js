import Proxy from './BaseProxy';
import axios from "axios";

class CartProxy extends Proxy {
	constructor(parameters = {}) {
		super('/', parameters);
	}

	getCargoPrice(data){
		return this.submit('get', `/get-cargo-price/`+data.city);
	}


}

export default CartProxy;
