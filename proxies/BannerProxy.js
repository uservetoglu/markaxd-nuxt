import Proxy from './BaseProxy';

class BannerProxy extends Proxy {
	constructor(parameters = {}) {
		super('banner', parameters);
	}

	all(){
		return this.submit('get', '/banner');
	}
}

export default BannerProxy;
