import Vue from 'vue';
import store from '@/store';
import axios from 'axios'

class BaseProxy{

	ApiUrl = process.env.api_url;
	BaseUrl = process.env.base_url;

	constructor(endpoint, parameters = {}) {
		this.endpoint = endpoint;
		this.parameters = parameters;
		if(process.client){
			this.headers = {
				session : localStorage.getItem('session')
			}
		}
	}

	getParameterString() {

		let queryString = Object.keys(this.parameters).map((key)=>{
			return key + '='+this.parameters[key];
		}).join('&');

		return queryString.length === 0 ? '' : `?${queryString}`;
	}

	submit(requestType, url, data = null){

		return new Promise((resolve, reject)=>{
			axios[requestType](this.ApiUrl+url+this.getParameterString(), data)
			.then(
				response =>{
					resolve(response.data);
				}
			)
			.catch(
				error=>{
					reject(error)
				}
			);
		})
	}

	all() {
		return this.submit('get', `/${this.endpoint}`);
	}
}

export default BaseProxy;
