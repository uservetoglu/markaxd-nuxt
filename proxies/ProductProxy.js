import Proxy from './BaseProxy';

class ProductProxy extends Proxy {
	constructor(parameters = {}) {
		super('product', parameters);
	}
	getProduct(slug){
		return this.submit('get', `/product/with-slug/`+slug);
	}
	newProducts(){
		return this.submit('get', `/product`);
	}
	discountProducts(){
		return this.submit('get', `/product-discount`);
	}
}

export default ProductProxy;
