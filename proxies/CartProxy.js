import Proxy from './BaseProxy';
import axios from "axios";

class CartProxy extends Proxy {
	constructor(parameters = {}) {
		super('/', parameters);
	}

	add(stock){
		return this.postSubmit('post', `/add-basket`, stock);
	}

	remove(stock){
		return this.submit('post', `/remove-basket`, stock);
	}

	removeCartItem(stock){
		return this.submit('post', `/remove-basket-item`, stock);
	}

	postSubmit(requestType, url, data = null){
		const headers = {
			'session': localStorage.getItem('session'),
			'Authorization': 'Bearer '+localStorage.getItem('token'),
		};

		return new Promise((resolve, reject)=>{
			axios.post(this.BaseUrl+url,  data, {headers})
				.then(response => {
						resolve(response.data);
					}
				)
				.catch(
					error=>{
						reject(error)
					}
				);
		})
	}

	submit(requestType, url, data = null){
		const headers = {
			'session': localStorage.getItem('session'),
			'Authorization': 'Bearer '+localStorage.getItem('token'),
		};
		return new Promise((resolve, reject)=>{
			axios[requestType](this.BaseUrl+url+this.getParameterString(), data,{headers})
			.then(response =>{
					resolve(response.data);
			})
			.catch(error=>{
					reject(error)
			 });
		})
	}

}

export default CartProxy;
