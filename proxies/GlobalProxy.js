import Proxy from './BaseProxy';

class GlobalProxy extends Proxy {
	constructor(parameters = {}) {
		super('/', parameters);
	}

	instagramFeed(){
		return this.submit('get', `/instagram`);
	}

	session(){
		return this.submit('get', `/session`);
	}
}

export default GlobalProxy;
