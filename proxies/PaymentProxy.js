import Proxy from './BaseProxy';
import axios from "axios";

class PaymentProxy extends Proxy {
	constructor(parameters = {}) {
		super('/', parameters);
	}

	payment(data){
		return this.submit('post', `/payment`, data);
	}

	installment(data){
		return this.submit('post', `/installment`, data);
	}

	paymentDetail(data){
		return this.submit('get', `/payment-detail/`+data.id);
	}

	submit(requestType, url, data = null){
		const headers = {
			'session': localStorage.getItem('session'),
			'Authorization': 'Bearer '+localStorage.getItem('token'),
		};

		data = requestType === 'post' ? data : {headers};

		return new Promise((resolve, reject)=>{
			axios[requestType](this.BaseUrl+url, data , {headers})
				.then(response => {
						resolve(response.data);
					}
				)
				.catch(
					error=>{
						reject(error)
					}
				);
		})
	}

}

export default PaymentProxy;
